"use strict";
exports.__esModule = true;
var Loginpage_1 = require("../Pages/Loginpage");
var Divisionpage_1 = require("../Pages/Divisionpage");
var common_1 = require("../Pages/common");
var Countries_1 = require("../Pages/Countries");
var VersionPage_1 = require("../Pages/VersionPage");
var Repeatedlyused = new common_1.common();
describe("Login", function () {
    var login = new Loginpage_1.Loginpage();
    it('Verify Valid crednetials', function () {
        login.OpenBrowser();
        login.verifyLogin();
        console.log('Login Successfull');
        login.ExplicitWait(5000);
        Repeatedlyused.ExplicitWait(5000);
    });
    it("Verify Landing page", function () {
        login.VeiryfLandingapage();
        console.log("Successfully Navigated to SELECT DIVISION");
    });
});
describe("Verify Division", function () {
    var division = new Divisionpage_1.Divisionpage();
    it("Verify Add Division", function () {
        division.AddDivision();
        division.VerifyTitle();
        console.log("Successfully Navigated to ADD DIVISION");
        Repeatedlyused.ExplicitWait(5000);
    });
    it("verify create Division", function () {
        division.VerifyCreateDivision();
        console.log("Successfully created a New Division");
    });
    it("Verify Edit Division", function () {
        division.VerifyEditDivision();
    });
    it("Verify View Division", function () {
        division.VerifySelectDivision();
    });
});
describe("Verify Country", function () {
    var country = new Countries_1.Countries();
    it("verify Add Country", function () {
        country.VerifyCountry();
        country.VerifyAddCountry();
    });
    it("verify Edit country", function () {
        country.VerifyEditCountry();
    });
});
describe("Verify Versions", function () {
    var version = new VersionPage_1.VersionPage();
    it("Verify Add Version", function () {
        version.VerifyVersion();
        version.VerifyAddVersion();
    });
    it("verify Edit Version", function () {
        version.VerifyEditVersion();
    });
});
//# sourceMappingURL=NopicOncology.js.map