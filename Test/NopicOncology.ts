import { browser, element, by, protractor,  } from "protractor";
import { Loginpage } from "../Pages/Loginpage";
import { Divisionpage } from "../Pages/Divisionpage";
import { common } from "../Pages/common";
import { Countries } from "../Pages/Countries";
import { VersionPage } from "../Pages/VersionPage";
var Repeatedlyused = new common()
describe("Login", () => {

    var login = new Loginpage();

    it('Verify Valid crednetials', () => {


        login.OpenBrowser()
        login.verifyLogin()
        console.log('Login Successfull')
        login.ExplicitWait(5000)
        Repeatedlyused.ExplicitWait(5000)

    });

    it("Verify Landing page", () => {
        login.VeiryfLandingapage()
        console.log("Successfully Navigated to SELECT DIVISION")
    });
})

describe("Verify Division", () => {
    var division = new Divisionpage()
    
   

    it("Verify Add Division", () => {
        division.AddDivision();
        division.VerifyTitle();
        console.log("Successfully Navigated to ADD DIVISION")
        Repeatedlyused.ExplicitWait(5000)
    })

    it("verify create Division", ()=>{
        division.VerifyCreateDivision()
        console.log("Successfully created a New Division")
       
        
    })

    it("Verify Edit Division", ()=> {
          division.VerifyEditDivision()
    })

    it("Verify View Division", ()=> {
        division.VerifySelectDivision()
    })
})

describe("Verify Country",()=> {
    var country = new Countries()
    it("verify Add Country", ()=> {
         country.VerifyCountry()
         country.VerifyAddCountry()
    })

    it("verify Edit country", ()=> {
        country.VerifyEditCountry()
    })
})

describe("Verify Versions", ()=> {
    var version = new VersionPage()
    it("Verify Add Version",()=> {
         version.VerifyVersion()
         version.VerifyAddVersion()
    })

    it("verify Edit Version", ()=>{
        version.VerifyEditVersion()
    })
})