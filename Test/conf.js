"use strict";
exports.__esModule = true;
var protractor_1 = require("protractor");
var jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
exports.config = {
    framework: 'jasmine',
    jasmineNodeOpts: {
        defaultTimeoutInterval: 2500000
    },
    directConnect: true,
    onPrepare: function () {
        protractor_1.browser.manage().window().maximize(); // maximize the browser before executing the feature files
        // Assign the test reporter to each running instance
        jasmine.getEnv().addReporter(new jasmine2HtmlReporter({
            savePath: './Reports',
            fixedScreenshotName: true,
            fileNamePrefix: 'NopicReport'
        }));
    },
    capabilities: {
        'browserName': 'chrome'
    },
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['NopicOncology.js']
    // Assign the test reporter to each running instance
};
//# sourceMappingURL=conf.js.map