"use strict";
exports.__esModule = true;
var protractor_1 = require("protractor");
var Loginpage = /** @class */ (function () {
    function Loginpage() {
        this.json = require('../json/Login_locators.json');
        this.UserName = protractor_1.element(protractor_1.by.id(this.json.locators.UserName.id));
        this.Password = protractor_1.element(protractor_1.by.id(this.json.locators.Password.id));
        this.submit = protractor_1.element(protractor_1.by.id(this.json.locators.submit.id));
        this.Homepage = protractor_1.element(protractor_1.by.linkText(this.json.locators.Homepage.linkText));
    }
    Loginpage.prototype.OpenBrowser = function () {
        protractor_1.browser.get(this.json.siteURL);
    };
    Loginpage.prototype.verifyLogin = function () {
        this.UserName.sendKeys(this.json.locators.UserInformation1.Information1);
        this.Password.sendKeys(this.json.locators.UserInformation2.Information2);
        this.submit.click();
    };
    Loginpage.prototype.VeiryfLandingapage = function () {
        var _this = this;
        this.Homepage.getText().then(function (text) {
            expect(text).toEqual(_this.json.locators.Expectedtitle.Title);
        });
    };
    Loginpage.prototype.ExplicitWait = function (ms) {
        protractor_1.browser.sleep(ms);
    };
    return Loginpage;
}());
exports.Loginpage = Loginpage;
//# sourceMappingURL=Loginpage.js.map