"use strict";
exports.__esModule = true;
var protractor_1 = require("protractor");
var common = /** @class */ (function () {
    function common() {
    }
    common.prototype.ExplicitWait = function (ms) {
        protractor_1.browser.sleep(ms);
    };
    common.prototype.Alert = function (ms) {
        var Ec = protractor_1.protractor.ExpectedConditions;
        protractor_1.browser.wait(Ec.alertIsPresent(), ms);
    };
    common.prototype.ScrollUp = function () {
        protractor_1.browser.executeScript("window.scrollBy(0,-1000)");
    };
    return common;
}());
exports.common = common;
//# sourceMappingURL=common.js.map