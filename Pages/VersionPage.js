"use strict";
exports.__esModule = true;
var protractor_1 = require("protractor");
var common_1 = require("../Pages/common");
var RepeatdelyUsed = new common_1.common();
var VersionPage = /** @class */ (function () {
    function VersionPage() {
        this.json = require('../Json/Version_locators.json');
        this.randomstring = require("randomstring");
        this.Homepage = protractor_1.element(protractor_1.by.xpath(this.json.locators.Dashboard.Xpath));
        this.Version = protractor_1.element(protractor_1.by.xpath(this.json.locators.VersionBtn.Xpath));
        this.ActTitle = protractor_1.element(protractor_1.by.xpath(this.json.locators.ActLandingPage.Xpath));
        this.AddVersion = protractor_1.element(protractor_1.by.xpath(this.json.locators.AddBtn.Xpath));
        this.VersionTitle = protractor_1.element(protractor_1.by.xpath(this.json.locators.ActTitle.Xpath));
        this.ImportData = protractor_1.element(protractor_1.by.xpath(this.json.locators.Selectoption.Xpath));
        this.SelectVersion = protractor_1.element(protractor_1.by.xpath(this.json.locators.SelectVersion.Xpath));
        this.Name = protractor_1.element(protractor_1.by.xpath(this.json.locators.VersionName.Xpath));
        this.Country = protractor_1.element(protractor_1.by.xpath(this.json.locators.SelectCountries.Xpath));
        this.ChooseCountry = protractor_1.element(protractor_1.by.xpath(this.json.locators.AssignCountry.Xpath));
        this.VersionDescription = protractor_1.element(protractor_1.by.id(this.json.locators.Description.id));
        this.Submit = protractor_1.element(protractor_1.by.xpath(this.json.locators.Submit.Xpath));
        this.ActAlert = protractor_1.element(protractor_1.by.xpath(this.json.locators.ActualSuccess.Xpath));
        this.Back = protractor_1.element(protractor_1.by.xpath(this.json.locators.BackBtn.Xpath));
        this.Search = protractor_1.element(protractor_1.by.xpath(this.json.locators.Filter.Xpath));
        this.EditVersion = protractor_1.element(protractor_1.by.xpath(this.json.locators.EditIcon.Xpath));
        this.StatusVersion = protractor_1.element(protractor_1.by.xpath(this.json.locators.StatusIcon.Xpath));
        this.SelectStatus = protractor_1.element(protractor_1.by.xpath(this.json.locators.Status.Xpath));
        this.ConfirmBtn = protractor_1.element(protractor_1.by.xpath(this.json.locators.SelectConfirm.Xpath));
        this.VersionAlert = protractor_1.element(protractor_1.by.xpath(this.json.locators.ActualAlert.Xpath));
    }
    VersionPage.prototype.VerifyVersion = function () {
        var _this = this;
        this.Homepage.click();
        this.Version.click();
        RepeatdelyUsed.ExplicitWait(5000);
        this.ActTitle.getText().then(function (text) {
            expect(text).toBe(_this.json.locators.ExpLandingPage.Message);
        });
    };
    VersionPage.prototype.VerifyAddVersion = function () {
        var _this = this;
        this.AddVersion.click();
        this.VersionTitle.getText().then(function (text) {
            expect(text).toBe(_this.json.locators.ExpTitle.Title);
        });
        this.ImportData.click();
        this.SelectVersion.click();
        this.Name.sendKeys(this.randomstring.generate(5));
        this.Country.click();
        this.ChooseCountry.click();
        this.VersionDescription.sendKeys(this.randomstring.generate(7));
        this.Submit.click();
        RepeatdelyUsed.ExplicitWait(5000);
        this.ActAlert.getText().then(function (text) {
            expect(text).toBe(_this.json.locators.ExpectedSuccess.Alert);
        });
        RepeatdelyUsed.ExplicitWait(5000);
    };
    VersionPage.prototype.VerifyEditVersion = function () {
        var _this = this;
        RepeatdelyUsed.ScrollUp();
        this.Back.click();
        this.Search.sendKeys(this.json.locators.SearchInfo.SearchText);
        this.EditVersion.click();
        this.StatusVersion.click().then(function () {
            _this.SelectStatus.click();
        });
        this.ConfirmBtn.click();
        RepeatdelyUsed.ExplicitWait(5000);
        this.VersionAlert.getText().then(function (text) {
            expect(text).toBe(_this.json.locators.ExpAlert.Title);
        });
    };
    return VersionPage;
}());
exports.VersionPage = VersionPage;
//# sourceMappingURL=VersionPage.js.map