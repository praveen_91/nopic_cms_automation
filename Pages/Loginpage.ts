import { browser, element, by } from "protractor";

export class Loginpage {
    json = require('../json/Login_locators.json');

    UserName = element(by.id(this.json.locators.UserName.id))
    Password = element(by.id(this.json.locators.Password.id))
    submit = element(by.id(this.json.locators.submit.id))
    Homepage = element(by.linkText(this.json.locators.Homepage.linkText))

    OpenBrowser() {
        browser.get(this.json.siteURL);
    }

    verifyLogin() {
        this.UserName.sendKeys(this.json.locators.UserInformation1.Information1)
        this.Password.sendKeys(this.json.locators.UserInformation2.Information2)
        this.submit.click()
    }

    VeiryfLandingapage() {
        this.Homepage.getText().then((text) => {
            expect(text).toEqual(this.json.locators.Expectedtitle.Title)
        })
    }

    ExplicitWait(ms) {
        browser.sleep(ms)
    }
}