import { browser, element, by, protractor } from "protractor";
import { common } from "../Pages/common";

var RepeatdelyUsed = new common()
export class Divisionpage {
    json = require('../json/Division_locators.json');

    Add = element(by.xpath(this.json.locators.Addbtn.Xpath))
    ExpTitle = element(by.xpath(this.json.locators.Actual.Xpath))
    Name = element(by.xpath(this.json.locators.AddName.Xpath))
    Code = element(by.xpath(this.json.locators.AddCode.Xpath))
    Submit = element(by.id(this.json.locators.AddSubmit.id))
    Alert = element(by.xpath(this.json.locators.Alert.Xpath))
    Back = element(by.xpath(this.json.locators.Backbtn.Xpath))
    Search = element(by.xpath(this.json.locators.Filter.Xpath))
    Edit = element(by.xpath(this.json.locators.EditIcon.Xpath))
    Editname= element(by.xpath(this.json.locators.EditName.Xpath))
    Editcode = element(by.xpath(this.json.locators.EditCode.Xpath))
    View = element(by.xpath(this.json.locators.SelectIcon.Xpath))
    randomstring = require("randomstring");

    AddDivision() {
        this.Add.click()
    }
    VerifyTitle() {
        this.ExpTitle.getText().then((text) => {
            expect(text).toEqual(this.json.locators.Expected.title)
        })
    }

    VerifyCreateDivision() {

        this.Name.sendKeys(this.randomstring.generate(7));
        this.Code.sendKeys(this.randomstring.generate(2));
        this.Submit.click()
        RepeatdelyUsed.ExplicitWait(8000)
        this.Alert.getText().then((text) => {
            expect(text).toBe(this.json.locators.Message.Success)
        })
        this.Back.click()
        RepeatdelyUsed.ExplicitWait(5000)




    }

    VerifyEditDivision() {
        this.Search.sendKeys(this.json.locators.SearchInfo.SeacrhName);
        this.Edit.click();
        RepeatdelyUsed.ExplicitWait(5000)
        this.Editname.clear()
        this.Editname.sendKeys(this.json.locators.EditDivisionName.EditDivisionHeading)
        this.Editcode.clear()
        this.Editcode.sendKeys(this.json.locators.EditDivisionCode.EditDivisionNumber)
        this.Submit.click()
        this.Alert.getText().then((text) => {
            expect(text).toBe(this.json.locators.EditMessage.AlertMessage)
        })
        
    }

      VerifySelectDivision(){
        this.Back.click();
        RepeatdelyUsed.ExplicitWait(5000)
        this.View.click();

      }

}