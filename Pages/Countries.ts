import { browser, element, by, protractor } from "protractor";
import { common } from "../Pages/common";

var RepeatdelyUsed = new common()
export class Countries{
     json = require('../Json/Countries_locators.json')

     cntry = element(by.xpath(this.json.locators.countryBtn.Xpath))
     ActTitle = element(by.xpath(this.json.locators.ActualTitle.Xpath))
     AddCountry = element(by.xpath(this.json.locators.AddCntry.Xpath))
     ActCntry = element(by.xpath(this.json.locators.ActualCntry.Xpath))
     CntryName = element(by.xpath(this.json.locators.CountryName.Xpath))
     CntryCode = element(by.xpath(this.json.locators.CountryCode.Xpath))
     SuperRegion = element(by.xpath(this.json.locators.SelectRegion.Xpath))
     SuperContinent = element(by.xpath(this.json.locators.SelectContinent.Xpath))
     Submit = element(by.id(this.json.locators.SubmitBtn.id))
     ActSuccess = element(by.xpath(this.json.locators.ActualSuccess.Xpath))
     Back = element(by.xpath(this.json.locators.BackBtn.Xpath))
     Search = element(by.xpath(this.json.locators.Filter.Xpath))
     Edit = element(by.xpath(this.json.locators.EditIcon.Xpath))
     Editcntry = element(by.xpath(this.json.locators.EditName.Xpath))
     EditCode = element(by.xpath(this.json.locators.EditCode.Xpath))
     ActAlert = element(by.xpath(this.json.locators.ActualMessage.Xpath))
     randomstring = require("randomstring");
    
     

     VerifyCountry(){
         this.cntry.click()
         RepeatdelyUsed.ExplicitWait(5000)
         this.ActTitle.getText().then((text)=>{
             expect(text).toBe(this.json.locators.ExpectedTitle.title)
         })

     }

     VerifyAddCountry(){
             this.AddCountry.click()
             RepeatdelyUsed.ExplicitWait(5000)
             this.ActCntry.getText().then((text)=> {
                 expect(text).toBe(this.json.locators.ExpectedCntry.country)
             })
             RepeatdelyUsed.ExplicitWait(5000)
             this.CntryName.sendKeys(this.json.locators.CountryHeading.Name)
             this.CntryCode.sendKeys(this.randomstring.generate(2))
             this.SuperRegion.click()
             this.SuperContinent.click()
             this.Submit.click()
             RepeatdelyUsed.ExplicitWait(5000)
             this.ActSuccess.getText().then((text)=>{
                 expect(text).toBe(this.json.locators.ExpectedSuccess.Message)
             })

     }

     VerifyEditCountry(){
           this.Back.click()
           this.Search.sendKeys(this.json.locators.SearchInfo.SeacrhName)
           this.Edit.click()
           RepeatdelyUsed.ExplicitWait(5000)
           this.Editcntry.clear()
           this.Editcntry.sendKeys(this.json.locators.CountryHeading.Name)
           this.Submit.click()
           this.ActAlert.getText().then((text)=>{
               expect(text).toBe(this.json.locators.ExpectedMessage.Alert)
           })



     }

   

    
}