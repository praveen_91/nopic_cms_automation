import { browser, element, by, protractor } from "protractor";

export class common {
    

    ExplicitWait(ms) {
        browser.sleep(ms)
    }

    Alert(ms){
        var Ec = protractor.ExpectedConditions;
        browser.wait(Ec.alertIsPresent(),ms)
    }

    ScrollUp() {
        browser.executeScript("window.scrollBy(0,-1000)")
    }
}