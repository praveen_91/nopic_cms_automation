"use strict";
exports.__esModule = true;
var protractor_1 = require("protractor");
var common_1 = require("../Pages/common");
var RepeatdelyUsed = new common_1.common();
var Divisionpage = /** @class */ (function () {
    function Divisionpage() {
        this.json = require('../json/Division_locators.json');
        this.Add = protractor_1.element(protractor_1.by.xpath(this.json.locators.Addbtn.Xpath));
        this.ExpTitle = protractor_1.element(protractor_1.by.xpath(this.json.locators.Actual.Xpath));
        this.Name = protractor_1.element(protractor_1.by.xpath(this.json.locators.AddName.Xpath));
        this.Code = protractor_1.element(protractor_1.by.xpath(this.json.locators.AddCode.Xpath));
        this.Submit = protractor_1.element(protractor_1.by.id(this.json.locators.AddSubmit.id));
        this.Alert = protractor_1.element(protractor_1.by.xpath(this.json.locators.Alert.Xpath));
        this.Back = protractor_1.element(protractor_1.by.xpath(this.json.locators.Backbtn.Xpath));
        this.Search = protractor_1.element(protractor_1.by.xpath(this.json.locators.Filter.Xpath));
        this.Edit = protractor_1.element(protractor_1.by.xpath(this.json.locators.EditIcon.Xpath));
        this.Editname = protractor_1.element(protractor_1.by.xpath(this.json.locators.EditName.Xpath));
        this.Editcode = protractor_1.element(protractor_1.by.xpath(this.json.locators.EditCode.Xpath));
        this.View = protractor_1.element(protractor_1.by.xpath(this.json.locators.SelectIcon.Xpath));
        this.randomstring = require("randomstring");
    }
    Divisionpage.prototype.AddDivision = function () {
        this.Add.click();
    };
    Divisionpage.prototype.VerifyTitle = function () {
        var _this = this;
        this.ExpTitle.getText().then(function (text) {
            expect(text).toEqual(_this.json.locators.Expected.title);
        });
    };
    Divisionpage.prototype.VerifyCreateDivision = function () {
        var _this = this;
        this.Name.sendKeys(this.randomstring.generate(7));
        this.Code.sendKeys(this.randomstring.generate(2));
        this.Submit.click();
        RepeatdelyUsed.ExplicitWait(8000);
        this.Alert.getText().then(function (text) {
            expect(text).toBe(_this.json.locators.Message.Success);
        });
        this.Back.click();
        RepeatdelyUsed.ExplicitWait(5000);
    };
    Divisionpage.prototype.VerifyEditDivision = function () {
        var _this = this;
        this.Search.sendKeys(this.json.locators.SearchInfo.SeacrhName);
        this.Edit.click();
        RepeatdelyUsed.ExplicitWait(5000);
        this.Editname.clear();
        this.Editname.sendKeys(this.json.locators.EditDivisionName.EditDivisionHeading);
        this.Editcode.clear();
        this.Editcode.sendKeys(this.json.locators.EditDivisionCode.EditDivisionNumber);
        this.Submit.click();
        this.Alert.getText().then(function (text) {
            expect(text).toBe(_this.json.locators.EditMessage.AlertMessage);
        });
    };
    Divisionpage.prototype.VerifySelectDivision = function () {
        this.Back.click();
        RepeatdelyUsed.ExplicitWait(5000);
        this.View.click();
    };
    return Divisionpage;
}());
exports.Divisionpage = Divisionpage;
//# sourceMappingURL=Divisionpage.js.map