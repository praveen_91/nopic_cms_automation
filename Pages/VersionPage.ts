import { browser, element, by, protractor } from "protractor";
import { common } from "../Pages/common";

var RepeatdelyUsed = new common()

export class VersionPage{
    json = require('../Json/Version_locators.json')

    randomstring = require("randomstring");

    Homepage = element(by.xpath(this.json.locators.Dashboard.Xpath))
    Version = element(by.xpath(this.json.locators.VersionBtn.Xpath))
    ActTitle = element(by.xpath(this.json.locators.ActLandingPage.Xpath))
    AddVersion = element(by.xpath(this.json.locators.AddBtn.Xpath))
    VersionTitle = element(by.xpath(this.json.locators.ActTitle.Xpath))
    ImportData = element(by.xpath(this.json.locators.Selectoption.Xpath))
    SelectVersion = element(by.xpath(this.json.locators.SelectVersion.Xpath))
    Name = element(by.xpath(this.json.locators.VersionName.Xpath))
    Country = element(by.xpath(this.json.locators.SelectCountries.Xpath))
    ChooseCountry = element(by.xpath(this.json.locators.AssignCountry.Xpath))
    VersionDescription = element(by.id(this.json.locators.Description.id))
    Submit = element(by.xpath(this.json.locators.Submit.Xpath))
    ActAlert = element(by.xpath(this.json.locators.ActualSuccess.Xpath))
    Back = element(by.xpath(this.json.locators.BackBtn.Xpath))
    Search = element(by.xpath(this.json.locators.Filter.Xpath))
    EditVersion = element(by.xpath(this.json.locators.EditIcon.Xpath))
    StatusVersion = element(by.xpath(this.json.locators.StatusIcon.Xpath))
    SelectStatus = element(by.xpath(this.json.locators.Status.Xpath))
    ConfirmBtn = element(by.xpath(this.json.locators.SelectConfirm.Xpath))
    VersionAlert = element(by.xpath(this.json.locators.ActualAlert.Xpath))


    VerifyVersion(){
        this.Homepage.click()
        this.Version.click()
        RepeatdelyUsed.ExplicitWait(5000)
        this.ActTitle.getText().then((text)=>{
            expect(text).toBe(this.json.locators.ExpLandingPage.Message)
        })
        
    }

    VerifyAddVersion(){
        this.AddVersion.click()
        this.VersionTitle.getText().then((text)=>{
            expect(text).toBe(this.json.locators.ExpTitle.Title)
        })
        this.ImportData.click()
        this.SelectVersion.click()
        this.Name.sendKeys(this.randomstring.generate(5))
        this.Country.click()
        this.ChooseCountry.click()
        this.VersionDescription.sendKeys(this.randomstring.generate(7))
        this.Submit.click()
        RepeatdelyUsed.ExplicitWait(5000)
        this.ActAlert.getText().then((text)=>{
            expect(text).toBe(this.json.locators.ExpectedSuccess.Alert)
        })
        RepeatdelyUsed.ExplicitWait(5000)

    }

    VerifyEditVersion(){
        RepeatdelyUsed.ScrollUp()
        this.Back.click()
        this.Search.sendKeys(this.json.locators.SearchInfo.SearchText)
        this.EditVersion.click()
        this.StatusVersion.click().then(()=>{
            this.SelectStatus.click()
        })
        this.ConfirmBtn.click()
        RepeatdelyUsed.ExplicitWait(5000)
        this.VersionAlert.getText().then((text)=>{
            expect(text).toBe(this.json.locators.ExpAlert.Title)
        })

        
    }
}