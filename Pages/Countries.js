"use strict";
exports.__esModule = true;
var protractor_1 = require("protractor");
var common_1 = require("../Pages/common");
var RepeatdelyUsed = new common_1.common();
var Countries = /** @class */ (function () {
    function Countries() {
        this.json = require('../Json/Countries_locators.json');
        this.cntry = protractor_1.element(protractor_1.by.xpath(this.json.locators.countryBtn.Xpath));
        this.ActTitle = protractor_1.element(protractor_1.by.xpath(this.json.locators.ActualTitle.Xpath));
        this.AddCountry = protractor_1.element(protractor_1.by.xpath(this.json.locators.AddCntry.Xpath));
        this.ActCntry = protractor_1.element(protractor_1.by.xpath(this.json.locators.ActualCntry.Xpath));
        this.CntryName = protractor_1.element(protractor_1.by.xpath(this.json.locators.CountryName.Xpath));
        this.CntryCode = protractor_1.element(protractor_1.by.xpath(this.json.locators.CountryCode.Xpath));
        this.SuperRegion = protractor_1.element(protractor_1.by.xpath(this.json.locators.SelectRegion.Xpath));
        this.SuperContinent = protractor_1.element(protractor_1.by.xpath(this.json.locators.SelectContinent.Xpath));
        this.Submit = protractor_1.element(protractor_1.by.id(this.json.locators.SubmitBtn.id));
        this.ActSuccess = protractor_1.element(protractor_1.by.xpath(this.json.locators.ActualSuccess.Xpath));
        this.Back = protractor_1.element(protractor_1.by.xpath(this.json.locators.BackBtn.Xpath));
        this.Search = protractor_1.element(protractor_1.by.xpath(this.json.locators.Filter.Xpath));
        this.Edit = protractor_1.element(protractor_1.by.xpath(this.json.locators.EditIcon.Xpath));
        this.Editcntry = protractor_1.element(protractor_1.by.xpath(this.json.locators.EditName.Xpath));
        this.EditCode = protractor_1.element(protractor_1.by.xpath(this.json.locators.EditCode.Xpath));
        this.ActAlert = protractor_1.element(protractor_1.by.xpath(this.json.locators.ActualMessage.Xpath));
        this.randomstring = require("randomstring");
    }
    Countries.prototype.VerifyCountry = function () {
        var _this = this;
        this.cntry.click();
        RepeatdelyUsed.ExplicitWait(5000);
        this.ActTitle.getText().then(function (text) {
            expect(text).toBe(_this.json.locators.ExpectedTitle.title);
        });
    };
    Countries.prototype.VerifyAddCountry = function () {
        var _this = this;
        this.AddCountry.click();
        RepeatdelyUsed.ExplicitWait(5000);
        this.ActCntry.getText().then(function (text) {
            expect(text).toBe(_this.json.locators.ExpectedCntry.country);
        });
        RepeatdelyUsed.ExplicitWait(5000);
        this.CntryName.sendKeys(this.json.locators.CountryHeading.Name);
        this.CntryCode.sendKeys(this.randomstring.generate(2));
        this.SuperRegion.click();
        this.SuperContinent.click();
        this.Submit.click();
        RepeatdelyUsed.ExplicitWait(5000);
        this.ActSuccess.getText().then(function (text) {
            expect(text).toBe(_this.json.locators.ExpectedSuccess.Message);
        });
    };
    Countries.prototype.VerifyEditCountry = function () {
        var _this = this;
        this.Back.click();
        this.Search.sendKeys(this.json.locators.SearchInfo.SeacrhName);
        this.Edit.click();
        RepeatdelyUsed.ExplicitWait(5000);
        this.Editcntry.clear();
        this.Editcntry.sendKeys(this.json.locators.CountryHeading.Name);
        this.Submit.click();
        this.ActAlert.getText().then(function (text) {
            expect(text).toBe(_this.json.locators.ExpectedMessage.Alert);
        });
    };
    return Countries;
}());
exports.Countries = Countries;
//# sourceMappingURL=Countries.js.map